from django.shortcuts import render, redirect
from accounts.forms import LoginInForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.


def login_user(request):
    if request.method == "POST":
        form = LoginInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            conf_pass = form.cleaned_data["password_confirmation"]

            if password == conf_pass:
                new_user = User.objects.create_user(
                    username=username, password=password
                )
                new_user.save()
                login(request, new_user)
                return redirect("home")
    else:
        form = SignUpForm()
    context = {
        "sign_up_form": form,
    }
    return render(request, "accounts/signup.html", context)
